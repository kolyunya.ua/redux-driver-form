export const setBusinessData = (businessData) => ({
    type: "SET_BUSSINES_DATA",
    payload: businessData,
});

export const setCurrentStep = (currentStep) => ({
    type: "SET_CURRENT_STEP",
    payload: currentStep,
});

export const setDriverData = (driverData) => ({
    type: 'SET_DRIVER_DATA',
    payload: driverData
})

export const addAnotherDriver = (driverData) => ({
    type: 'ADD_ANOTHER_DRIVER',
    payload: driverData
})

export const setDriverCreated = (value) =>({
    type: 'SET_DRIVER_CREATED',
    payload: value
})

export const deleteDriver = (id) => ({
    type: 'DELETE_DRIVER',
    payload: id
})

export const editDriver = (driverObj) => ({
    type: 'EDIT_DRIVER',
    payload: driverObj
})

export const toggleAddDriverModalState = (value) => ({
    type: 'TOGGLE_ADD_DRIVER_MODAL_STATE',
    payload: value
})

export const toggleEditDriverModalState = (value) => ({
    type: 'TOGGLE_EDIT_DRIVER_MODAL_STATE',
    payload: value
})

export const setTruckName = (name) => ({
    type: 'SET_TRUCK_NAME',
    payload: name
})

export const setTreilerTruckName = (name) => ({
    type: 'SET_TREILER_TRUCK_NAME',
    payload: name
})

export const setTruckInfo = (truckInfo) => ({
    type: 'SET_TRUCK_INFO',
    payload: truckInfo
})

export const setVehicleCreated = (value) => ({
    type: 'SET_VEHICLE_CREATED',
    payload: value
})

export const deleteVehicle = (value) => ({
    type: "DELETE_VEHICLE",
    payload: value
})