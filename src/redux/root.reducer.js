import { combineReducers } from "redux";

// Reducers
import formData from "./reducers/form.reducer";

const rootReducer = combineReducers({
    formData: formData
})

export default rootReducer;