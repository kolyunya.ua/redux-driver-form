const makeid = (length) => {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

const initialDriver = makeid(5);


const initialState = {
    businessData: {
        zipcode: '',
        businessName: '',
        usdot: '',
        ownerName: '',
        businessMiles: '',
        businessType: '',
        businessDate: ''

    },
    driversData: {
        [initialDriver]: {
            driverFullName: '',
            driverBirthDay: '',
            driverLicenseNumber: '',
            driverLicenseState: '',
            driverMovingNumber: '',
            driverAccidentsNumber: ''
        }

    },
    truckData: {},
    truckName: '',
    treilerTruckName: '',
    current: 0,
    driverCreated: false,
    vehicleCreated: false,
    addDriverModalState: false,
    editDriverModalState: false
}
const formData = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_CURRENT_STEP':
            return {
                ...state,
                current: action.payload

            }
        case 'SET_BUSSINES_DATA':
            return {
                ...state,
                businessData: action.payload
            }
        case 'SET_DRIVER_DATA':
            const newDriver = Object.keys(state.driversData)[0];
            return {
                ...state,
                driversData: {
                    ...state.driversData,
                    [newDriver]: action.payload
                }
            }

        case 'ADD_ANOTHER_DRIVER':
            const driverId = makeid(5);
            return {
                ...state,
                driversData: {
                    ...state.driversData,
                    [driverId]: action.payload
                }
            }

        case 'SET_DRIVER_CREATED':
            return {
                ...state,
                driverCreated: action.payload
            }
        case 'DELETE_DRIVER':
            delete state.driversData[action.payload]
            if (Object.keys(state.driversData).length === 0) {
                return {
                    ...state,
                    driversData: {
                        [initialDriver]: {
                            driverFullName: '',
                            driverBirthDay: '',
                            driverLicenseNumber: '',
                            driverLicenseState: '',
                            driverMovingNumber: '',
                            driverAccidentsNumber: ''
                        }

                    },
                    driverCreated: false
                }
            }
            return {
                ...state,
                driversData: {
                    ...state.driversData
                }
            }
        case 'EDIT_DRIVER':
            return {
                ...state,
                driversData: {
                    ...state.driversData,
                    [action.payload.id]: action.payload.driverData
                }
            }
        case 'TOGGLE_ADD_DRIVER_MODAL_STATE':
            return {
                ...state,
                addDriverModalState: action.payload
            }
        case 'TOGGLE_EDIT_DRIVER_MODAL_STATE':
            return {
                ...state,
                editDriverModalState: action.payload
            }
        case 'SET_TRUCK_NAME':
            return {
                ...state,
                truckName: action.payload
            }
        case 'SET_TREILER_TRUCK_NAME':
            return {
                ...state,
                treilerTruckName: action.payload
            }
        case 'SET_TRUCK_INFO':
            const truckId = makeid(5)
            return {
                ...state,
                truckData: {
                    ...state.truckData,
                    [truckId]: action.payload
                }
            }
        case "SET_VEHICLE_CREATED":
            return {
                ...state,
                vehicleCreated: action.payload
            }
        case 'DELETE_VEHICLE':
            delete state.truckData[action.payload]
            if (Object.keys(state.truckData).length === 0) {
                return {
                    ...state,
                    truckData: {},
                    vehicleCreated: false
                }
            }
            return {
                ...state,
                truckData: {
                    ...state.truckData
                }
            }
        default:
            return state;
    }

}

export default formData;