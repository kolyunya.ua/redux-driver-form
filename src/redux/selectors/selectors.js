export const getDrivers = (state) => {
    let driversArray = [];
    const driversData = state.formData.driversData;
    for (var key in driversData) {
        driversArray.push({ ...driversData[key], id: key });
    }
    return driversArray;
}

export const getVehicles = (state) => {
    let vehiclesArray = [];
    const truckData = state.formData.truckData;
    for (var key in truckData) {
        vehiclesArray.push({ ...truckData[key], id: key });
    }
    return vehiclesArray;
}