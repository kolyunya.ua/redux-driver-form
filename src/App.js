import React from 'react';
import './App.css';
import MainForm from './components/MainForm'

const App = () =>{
  return (
    <div className="App">
      <MainForm />
    </div>
  );
}

export default App;
