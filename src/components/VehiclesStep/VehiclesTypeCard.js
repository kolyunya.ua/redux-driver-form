import React from 'react';
import { useDispatch } from 'react-redux';
import { setTruckName, setTreilerTruckName } from './../../redux/actions/form.actions'

const VehiclesTypeCard = (props) => {
    const dispatch = useDispatch();
    const handleClick = () => {
        props.setActiveState(props.index);
        if (props.type === 'main') {
            dispatch(setTruckName(props.title))
        }
        if (props.type === 'treilers') {
            dispatch(setTreilerTruckName(props.title))
        }
    }
    return (
        <div className={`vehicles-type-card ${props.active ? 'active' : {}}`} onClick={() => handleClick()}>
            <div className="vehicles-type-card__icon">
                <img src={props.icon} alt={props.title} />
            </div>
            <div className="vehicles-type-card__title">
                <h3>{props.title}</h3>
            </div>
        </div>
    )
}

export default VehiclesTypeCard;