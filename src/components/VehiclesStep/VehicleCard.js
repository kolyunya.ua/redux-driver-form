import React, { useState } from 'react';
import { useDispatch } from "react-redux";
import { deleteVehicle } from './../../redux/actions/form.actions';
// import EditVehicleModal from './EditVehicleModal';
import { Button } from 'antd'


const DriverCard = (props) => {
    const { truckName, treilerTruckName, makeOfVehicle, vehicleValue, yearOfVehicle, vehicleBody, id } = props.data;
    const dispatch = useDispatch();
    const [openModal, setOpenModal] = useState();
    return (
        <div className="driver-card">
            <div className="driver-card__title">
                <h2>{`${yearOfVehicle} ${makeOfVehicle}`}</h2>
            </div>
            <div className="driver-card__body">
                <div className="driver-card__body--icon">
                    <img src='images/driver-icon.png' alt="driver-card-icon" />
                </div>
                <ul className="driver-card__body--details">
                    <li>
                        <span>Year</span>
                        <span>{yearOfVehicle}</span>
                    </li>
                    <li>
                        <span>Make</span>
                        <span>{makeOfVehicle}</span>
                    </li>
                    <li>
                        <span>Type</span>
                        <span>{truckName} {`${treilerTruckName && "-" + treilerTruckName}`} {vehicleBody && "-" + vehicleBody}</span>
                    </li>
                    <li>
                        <span>Vehicle Value</span>
                        <span>{vehicleValue}</span>
                    </li>
                </ul>
            </div>
            <div className="driver-card__buttons">
                <Button type="primary" onClick={() => setOpenModal(true)}>Edit</Button>
                <Button type="primary" danger onClick={() => { dispatch(deleteVehicle(id)) }}>Delete</Button>
            </div>
            {/* <EditVehicleModal id={id} openModal={openModal} closeModal={setOpenModal} /> */}
        </div>
    )
}

export default DriverCard;