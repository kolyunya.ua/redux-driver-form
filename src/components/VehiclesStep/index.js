import React, { useState } from 'react';
import VehiclesTypeCard from './VehiclesTypeCard'
import { Form, Input, Row, Col, Button } from 'antd';
import { setTruckInfo, setVehicleCreated, setCurrentStep } from './../../redux/actions/form.actions'
import { useDispatch, useSelector } from 'react-redux';
import { getVehicles } from './../../redux/selectors/selectors'

import VehicleCard from './VehicleCard'
import AddVehicleModal from './AddVehicleModal'

const VehiclesStep = () => {
    const [activeMainType, setActiveMainType] = useState('');
    const [activeTreilerType, setActiveTreilerType] = useState('');
    const [state, setState] = useState('');
    const truckName = useSelector(state => state.formData.truckName);
    const treilerTruckName = useSelector(state => state.formData.treilerTruckName);
    const vehicleCreated = useSelector(state => state.formData.vehicleCreated)
    const truckData = useSelector(state => getVehicles(state))
    const current = useSelector(state => state.formData.current)
    const dispatch = useDispatch();

    const vehiclesTypes = [
        {
            title: 'Semi Truck',
            icon: 'images/semi-truck-icon.svg'
        },
        {
            title: 'Box Truck',
            icon: 'images/box-truck-icon.svg'
        },
        {
            title: 'Pickup Truck',
            icon: 'images/pick-up-icon.svg'
        },
        {
            title: 'Cargo Van',
            icon: 'images/cargo-van-icon.svg'
        },
        {
            title: 'Dump Truck',
            icon: 'images/dump-truck-icon.svg'
        },
        {
            title: 'Tow Truck',
            icon: 'images/tow-truck-icon.svg'
        },
        {
            title: 'Treiler Truck',
            icon: 'images/treiler-icon.svg'
        },
        {
            title: 'Other',
            icon: 'images/other-icon.svg'
        },
    ]

    const treilerTypes = [
        {
            title: 'Dry Van',
            icon: 'images/dry-van-icon.svg'
        },
        {
            title: 'Refrigerated',
            icon: 'images/refrigerated-icon.svg'
        },
        {
            title: 'Flatbed',
            icon: 'images/flatbed-icon.svg'
        },
        {
            title: 'Intermodal',
            icon: 'images/intermodal-icon.svg'
        },
        {
            title: 'Car Carrier',
            icon: 'images/car-carrier-icon.svg'
        },
        {
            title: 'Tank',
            icon: 'images/tank-icon.svg'
        },
        {
            title: 'Dump',
            icon: 'images/dump-icon.svg'
        },
        {
            title: 'Log',
            icon: 'images/log-icon.svg'
        },
        {
            title: 'Low Boy',
            icon: 'images/low-boy-icon.svg'
        },
    ]

    const inputHandler = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        setState({ ...state, [name]: value });
    }

    const saveTruckData = () => {
        dispatch(setTruckInfo({ ...state, truckName: truckName, treilerTruckName: activeMainType === 6 ? treilerTruckName : '' }))
        dispatch(setVehicleCreated(true))
    }

    const prevStep = () => {
        dispatch(setCurrentStep(current - 1))
    }

    const nextStep = () => {
        dispatch(setCurrentStep(current + 1))
    }

    return (
        <>
            {!vehicleCreated ? (
                <>
                    <div className="vehicles-type-container">
                        {vehiclesTypes.map((item, index) => {
                            return <VehiclesTypeCard title={item.title} icon={item.icon} key={index} index={index} active={activeMainType === index ? true : false} setActiveState={setActiveMainType} type="main" />
                        })}
                        {activeMainType === 6 && treilerTypes.map((item, index) => {
                            return <VehiclesTypeCard title={item.title} icon={item.icon} key={index} index={index} active={activeTreilerType === index ? true : false} setActiveState={setActiveTreilerType} type="treilers" />
                        }
                        )}
                    </div>
                    <div className="vehicle-add-info">
                        <Form
                            name="driverFormSingle"
                        >
                            <Row className="row">
                                <Col span={12} className="gutter-row">
                                    <Form.Item
                                        label="Make of Vehicle *"
                                        name="makeOfVehicle"
                                        rules={[{ required: true, message: 'Required field!' }]}
                                    >
                                        <Input name="makeOfVehicle" onChange={inputHandler} />
                                    </Form.Item>
                                </Col>
                                <Col span={12} className="gutter-row">
                                    <Form.Item
                                        label="Vehicle Value (optional)"
                                        name="vehicleValue"
                                    >
                                        <Input name="vehicleValue" onChange={inputHandler} />
                                    </Form.Item>
                                </Col>
                                <Col span={12} className="gutter-row">
                                    <Form.Item
                                        label="Year of Vehicle *"
                                        name="yearOfVehicle"
                                        rules={[{ required: true, message: 'Required field!' }]}
                                    >
                                        <Input name="yearOfVehicle" onChange={inputHandler} />
                                    </Form.Item>
                                </Col>
                                {activeMainType && activeMainType === 7 &&
                                    <Col span={12} className="gutter-row">
                                        <Form.Item
                                            label="Please describe the vehicle’s body style *"
                                            name="vehicleBody"
                                            rules={[{ required: true, message: 'Required field!' }]}
                                        >
                                            <Input name="vehicleBody" onChange={inputHandler} />
                                        </Form.Item>
                                    </Col>
                                }
                            </Row>
                            <Button onClick={saveTruckData}>Save and continue</Button>
                        </Form>
                    </div>
                </>
            )
                :
                (
                    <div className="drivers-list">
                        {truckData.map((item, index) => {
                            return <VehicleCard data={item} key={index} />
                        })}
                        <div className="drivers-list__buttons">
                            {/* <Button onClick={() => dispatch(toggleAddDriverModalState(true))}>Add another driver</Button> */}
                            <Button onClick={nextStep}>Continue</Button>
                            <Button onClick={prevStep}>Prev step</Button>
                        </div>
                        <AddVehicleModal />
                    </div>
                )
            }
        </>
    )
}

export default VehiclesStep;