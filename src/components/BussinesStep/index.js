import React, { useState } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { setBusinessData, setCurrentStep } from './../../redux/actions/form.actions';
import { Form, Input, Row, Col, DatePicker, Button } from 'antd';
import moment from 'moment';

const BussinesStep = () => {
    const [form] = Form.useForm();
    console.log(form)
    const dispatch = useDispatch();
    // const { validateFields } = props.form;
    const current = useSelector(state => state.formData.current)
    const businessData = useSelector(state => state.formData.businessData);
    // const [ form ] = useform(); 
    const [state, setState] = useState({
        zipcode: businessData.zipcode || '',
        businessName: businessData.businessName || '',
        usdot: businessData.usdot || '',
        ownerName: businessData.ownerName || '',
        businessMiles: businessData.businessMiles || '',
        businessType: businessData.businessType || '',
        businessDate: businessData.businessDate || ''
    });

    const inputHandler = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        setState({ ...state, [name]: value });
        console.log('state', state)
    }

    const saveBusinessData = () => {
        dispatch(setBusinessData(state))
        dispatch(setCurrentStep(current + 1));
    }




    return (
        <Form
            name="businessForm"
            form={form}
            initialValues={{
                zipcode: businessData.zipcode,
                businessName: businessData.businessName,
                usdot: businessData.usdot,
                ownerName: businessData.ownerName,
                businessMiles: businessData.businessMiles,
                businessType: businessData.businessType,
            }}
        >
            <Row className="row">
                <Col span={12} className="gutter-row">
                    <Form.Item
                        label="Business Zip Code*"
                        name="zipcode"
                        rules={[
                            {
                                required: true,
                                message: 'Required field!'
                            },
                            {
                                min: 5,
                                message: 'Please enter a valid five digit zip code.',
                            },
                            {
                                max: 5,
                                message: 'Please enter a valid five digit zip code.',
                            },
                        ]}
                    >
                        <Input name="zipcode" onChange={inputHandler} />
                    </Form.Item>
                </Col>
                <Col span={12} className="gutter-row">
                    <Form.Item
                        label="Business Name *"
                        name="businessName"
                        rules={[{ required: true, message: 'Required field!' }]}
                    >
                        <Input name="businessName" onChange={inputHandler} />
                    </Form.Item>
                </Col>
            </Row>
            <Row className="row">
                <Col span={12} className="gutter-row">
                    <Form.Item
                        label="USDOT# *"
                        name="usdot"
                        rules={[
                            {
                                required: true,
                                message: 'Required field!'
                            },
                            {
                                min: 7,
                                message: 'USDOT must have 7 symbols',
                            },
                            {
                                max: 7,
                                message: 'USDOT must have 7 symbols',
                            },]}
                    >
                        <Input name="usdot" onChange={inputHandler} />
                    </Form.Item>
                </Col>
                <Col span={12} className="gutter-row">
                    <Form.Item
                        label="Business Owner’s Full Name*"
                        name="ownerName"
                        rules={[{ required: true, message: 'Required field!' }]}
                    >
                        <Input name="ownerName" onChange={inputHandler} />
                    </Form.Item>
                </Col>
            </Row>
            <Row className="row">
                <Col span={12} className="gutter-row">
                    <Form.Item
                        label="What's the furthest distance you travel one-way in miles? *"
                        name="businessMiles"
                        rules={[{ required: true, message: 'Required field!' }]}
                    >
                        <Input name="businessMiles" onChange={inputHandler} />
                    </Form.Item>
                </Col>
                <Col span={12} className="gutter-row">
                    <Form.Item
                        label="Please describe your business and the type(s) of cargo you haul *"
                        name="businessType"
                        rules={[{ required: true, message: 'Required field!' }]}
                    >
                        <Input name="businessType" onChange={inputHandler} />
                    </Form.Item>
                </Col>
            </Row>
            <Row className="row">
                <Col span={12} className="gutter-row">
                    <Form.Item
                        label="What day would you like to start your policy?*"
                        name="businessDate"
                        rules={[{ required: true, message: 'Required field!' }]}
                    >

                        <DatePicker defaultValue={businessData.businessDate ? moment(businessData.businessDate) : ''} onChange={(value, dateString) => { setState({ ...state, businessDate: dateString }) }} />
                    </Form.Item>
                </Col>
            </Row>
            <Button className="primary" onClick={saveBusinessData}>Save and Continue</Button>
        </Form>

    )
}

export default BussinesStep;