import React, { useState } from 'react';
import { useDispatch } from "react-redux";
import { deleteDriver } from './../../redux/actions/form.actions';
import EditDriverModal from './EditDriverModal';
import { Button } from 'antd'


const DriverCard = (props) => {
    const { driverFullName, driverBirthDay, driverLicenseState, driverLicenseNumber, driverMovingNumber, driverAccidentsNumber, id } = props.driverData;
    const dispatch = useDispatch();
    const [openModal, setOpenModal] = useState();
    return (
        <div className="driver-card">
            <div className="driver-card__title">
                <h2>{driverFullName}</h2>
            </div>
            <div className="driver-card__body">
                <div className="driver-card__body--icon">
                    <img src='images/driver-icon.png' alt="driver-card-icon" />
                </div>
                <ul className="driver-card__body--details">
                    <li>
                        <span>Date of Birth</span>
                        <span>{driverBirthDay}</span>
                    </li>
                    <li>
                        <span>License Sate</span>
                        <span>{driverLicenseState}</span>
                    </li>
                    <li>
                        <span>License Number</span>
                        <span>{driverLicenseNumber}</span>
                    </li>
                    <li>
                        <span>Number of moving violations in the past 3 years</span>
                        <span>{driverMovingNumber}</span>
                    </li>
                    <li>
                        <span>Number of accident in the past 3 years</span>
                        <span>{driverAccidentsNumber}</span>
                    </li>
                </ul>
            </div>
            <div className="driver-card__buttons">
                <Button type="primary" onClick={() => setOpenModal(true)}>Edit</Button>
                <Button type="primary" danger onClick={() => { dispatch(deleteDriver(id)) }}>Delete</Button>
            </div>
            <EditDriverModal id={id} openModal={openModal} closeModal={setOpenModal} />
        </div>
    )
}

export default DriverCard;