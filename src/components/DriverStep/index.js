import React, { useState } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { setDriverData, setCurrentStep, setDriverCreated, toggleAddDriverModalState } from './../../redux/actions/form.actions';
import { Form, Input, Row, Col, DatePicker, Select, Button } from 'antd';
import { getDrivers } from './../../redux/selectors/selectors'
import moment from 'moment';
import AddDriverModal from './AddDriverModal'

import DriverCard from './DriverCard'

const { Option } = Select;

const DriverStep = () => {
    const dispatch = useDispatch();
    const current = useSelector(state => state.formData.current)
    const driverCreated = useSelector(state => state.formData.driverCreated)
    const driversData = useSelector(state => getDrivers(state));
    const [state, setState] = useState({
        driverFullName: driversData.driverFullName || '',
        driverBirthDay: driversData.driverBirthDay || '',
        driverLicenseNumber: driversData.driverLicenseNumber || '',
        driverLicenseState: driversData.driverLicenseState || '',
        driverMovingNumber: driversData.driverMovingNumber || '',
        driverAccidentsNumber: driversData.driverAccidentsNumber || ''
    });

    const licenceStateList = [
        'Alabama',
        'Alaska',
        'Arkanzas',
        'California',
        'New Mexico',
    ]
    const movingNumbersList = [
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7+'
    ]

    const inputHandler = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        setState({ ...state, [name]: value });
    }

    const selectHandler = (value, e) => {
        let name = e.name;
        setState({ ...state, [name]: value })
    }


    const saveFirstDriverData = () => {
        dispatch(setDriverData(state));
        dispatch(setDriverCreated(true))
    }

    const prevStep = () => {
        dispatch(setCurrentStep(current - 1))
    }

    const nextStep = () => {
        dispatch(setCurrentStep(current + 1))
    }
    // const renderCardDriver = () => {
    //     let targetObj = [];
    //     for (var key in driversData) {
    //         targetObj.push({ ...driversData[key], id: key });
    //     }
    //     return targetObj;
    // }


    return (
        <>
            {!driverCreated &&
                <Form
                    name="FormSingle"
                >
                    <Row className="row">
                        <Col span={12} className="gutter-row">
                            <Form.Item
                                label="Driver’s Full Name *"
                                name="driverFullName"
                                rules={[{ required: true, message: 'Required field!' }]}
                            >
                                <Input name="driverFullName" onChange={inputHandler} />
                            </Form.Item>
                        </Col>
                        <Col span={12} className="gutter-row">
                            <Form.Item
                                label="Date of Birth *"
                                name="driverBirthDay"
                                rules={[{ required: true, message: 'Required field!' }]}
                            >
                                <DatePicker onChange={(value, dateString) => { setState({ ...state, driverBirthDay: dateString }); }} defaultValue={driversData.driverBirthDay ? moment(driversData.driverBirthDay) : ''} />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row className="row">
                        <Col span={12} className="gutter-row">
                            <Form.Item
                                label="License Number *"
                                name="driverLicenseNumber"
                            >
                                <Input name="driverLicenseNumber" onChange={inputHandler} />
                            </Form.Item>
                        </Col>
                        <Col span={12} className="gutter-row">
                            <Form.Item
                                label="License state *"
                                name="driverLicenseState"
                                rules={[{ required: true, message: 'Required field!' }]}
                            >
                                <Select name="driverLicenseState" onChange={selectHandler} placeholder="Choose License State">
                                    {licenceStateList.map((item, index) => {
                                        return <Option name="driverLicenseState" value={item} key={index}>{item}</Option>
                                    })}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row className="row">
                        <Col span={12} className="gutter-row">
                            <Form.Item
                                label="Number of moving violations in the past 3 years *"
                                name="driverMovingNumber"
                                rules={[{ required: true, message: 'Required field!' }]}
                            >
                                <Select name="driverMovingNumber" onChange={selectHandler} placeholder="Choose Number of Violations">
                                    {movingNumbersList.map((item, index) => {
                                        return <Option name="driverMovingNumber" value={item} key={index}>{item}</Option>
                                    })}
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col span={12} className="gutter-row">
                            <Form.Item
                                label="Number of accidents in the past 3 years *"
                                name="driverAccidentsNumber"
                                rules={[{ required: true, message: 'Required field!' }]}
                            >
                                <Select name="driverAccidentsNumber" onChange={selectHandler} placeholder="Choose Number of Accidents">
                                    {movingNumbersList.map((item, index) => {
                                        return <Option name="driverAccidentsNumber" value={item} key={index}>{item}</Option>
                                    })}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                    <Button onClick={saveFirstDriverData}>Save and Continue</Button>
                </Form>
            }

            {driverCreated && (
                <div className="drivers-list">
                    {driversData.map((item, index) => {
                        return <DriverCard driverData={item} key={index} />
                    })}
                    <div className="drivers-list__buttons">
                        <Button onClick={() => dispatch(toggleAddDriverModalState(true))}>Add another driver</Button>
                        <Button onClick={nextStep}>Continue</Button>
                        <Button onClick={prevStep}>Prev step</Button>
                    </div>
                    <AddDriverModal />
                </div>
            )
            }
        </>
    )
}

export default DriverStep;