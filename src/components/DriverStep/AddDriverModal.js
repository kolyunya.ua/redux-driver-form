import React, { useState } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { toggleAddDriverModalState, addAnotherDriver } from './../../redux/actions/form.actions';
import { Form, Input, Row, Col, DatePicker, Select, Button } from 'antd';
import Modal from 'react-modal';


const { Option } = Select;

const AddDriverModal = () => {
    const modalState = useSelector(state => state.formData.addDriverModalState);
    const dispatch = useDispatch();
    const [state, setState] = useState({
        driverFullName: '',
        driverBirthDay: '',
        driverLicenseNumber: '',
        driverLicenseState: '',
        driverMovingNumber: '',
        driverAccidentsNumber: ''
    });
    const licenceStateList = [
        'Alabama',
        'Alaska',
        'Arkanzas',
        'California',
        'New Mexico',
    ]
    const movingNumbersList = [
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7+'
    ]

    const inputHandler = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        setState({ ...state, [name]: value });
    }

    const selectHandler = (value, e) => {
        let name = e.name;
        setState({ ...state, [name]: value })
    }

    const addNewDriver = () => {
        dispatch(addAnotherDriver(state));
        dispatch(toggleAddDriverModalState(false))
    }
    Modal.setAppElement('#root')
    return (
        <Modal
            isOpen={modalState}
            onRequestClose={() => dispatch(toggleAddDriverModalState(false))}
        >
            <Form
                name="driverFormSingle"
            >
                <Row className="row">
                    <Col span={12} className="gutter-row">
                        <Form.Item
                            label="Driver’s Full Name *"
                            name="driverFullName"
                            rules={[{ required: true, message: 'Required field!' }]}
                        >
                            <Input name="driverFullName" onChange={inputHandler} />
                        </Form.Item>
                    </Col>
                    <Col span={12} className="gutter-row">
                        <Form.Item
                            label="Date of Birth *"
                            name="driverBirthDay"
                            rules={[{ required: true, message: 'Required field!' }]}
                        >
                            <DatePicker onChange={(value, dateString) => { setState({ ...state, driverBirthDay: dateString }); }} />
                        </Form.Item>
                    </Col>
                </Row>
                <Row className="row">
                    <Col span={12} className="gutter-row">
                        <Form.Item
                            label="License Number *"
                            name="driverLicenseNumber"
                        >
                            <Input name="driverLicenseNumber" onChange={inputHandler} />
                        </Form.Item>
                    </Col>
                    <Col span={12} className="gutter-row">
                        <Form.Item
                            label="License state *"
                            name="driverLicenseState"
                            rules={[{ required: true, message: 'Required field!' }]}
                        >
                            <Select name="driverLicenseState" onChange={selectHandler} placeholder="Choose License State">
                                {licenceStateList.map((item, index) => {
                                    return <Option name="driverLicenseState" value={item} key={index}>{item}</Option>
                                })}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row className="row">
                    <Col span={12} className="gutter-row">
                        <Form.Item
                            label="Number of moving violations in the past 3 years *"
                            name="driverMovingNumber"
                            rules={[{ required: true, message: 'Required field!' }]}
                        >
                            <Select name="driverMovingNumber" onChange={selectHandler} placeholder="Choose Number of Violations">
                                {movingNumbersList.map((item, index) => {
                                    return <Option name="driverMovingNumber" value={item} key={index}>{item}</Option>
                                })}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={12} className="gutter-row">
                        <Form.Item
                            label="Number of accidents in the past 3 years *"
                            name="driverAccidentsNumber"
                            rules={[{ required: true, message: 'Required field!' }]}
                        >
                            <Select name="driverAccidentsNumber" onChange={selectHandler} placeholder="Choose Number of Accidents">
                                {movingNumbersList.map((item, index) => {
                                    return <Option name="driverAccidentsNumber" value={item} key={index}>{item}</Option>
                                })}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Button onClick={() => dispatch(toggleAddDriverModalState(false))}>Cancel</Button>
                <Button onClick={addNewDriver}>Save</Button>
            </Form>
        </Modal>
    )
}

export default AddDriverModal;