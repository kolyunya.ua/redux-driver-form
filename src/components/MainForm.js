/* eslint-disable import/first */ 
import React from 'react';;
import { useSelector } from "react-redux";
import { Steps } from 'antd';
import BussinesStep from './BussinesStep';
import DriverStep from './DriverStep';
import VehiclesStep from './VehiclesStep'
import 'antd/dist/antd.css';

const { Step } = Steps;

const MainForm = () => {
    const current = useSelector(state => state.formData.current)

    const steps = [
        {
            title: 'Business',
            content: <BussinesStep />,
        },
        {
            title: 'Drivers',
            content: <DriverStep />
        },
        {
            title: 'Vehicles',
            content: <VehiclesStep />
        },
        {
            title: 'Extra',
            content: 'Third content'
        },
        {
            title: 'Summary',
            content: 'Third content'
        },
        {
            title: 'Submit',
            content: 'Third content'
        },
    ];

    return (
        <div className="container">
            <Steps current={current} style={{ 'marginBottom': '60px' }}>
                {steps.map(item => (
                    <Step key={item.title} title={item.title} />
                ))}
            </Steps>
            <div className="steps-content">{steps[current].content}</div>
            <div className="steps-action">
            </div>
        </div>
    )
}

export default MainForm;